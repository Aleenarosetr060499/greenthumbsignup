package com.example.greenthumbsignup

data class UsersList (val status: String? = null,
                      val message: String? = null,
                      val date: Users? = null)
data class  Errors(val status: String?,val message: String?)
