package com.example.greenthumbsignup

data class StateResponse (
    val data: List<StateDetails>,
    val message: String,
    val status: String
    )