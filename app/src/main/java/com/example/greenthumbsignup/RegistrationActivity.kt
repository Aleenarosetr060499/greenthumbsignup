package com.example.greenthumbsignup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registration.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegistrationActivity : AppCompatActivity(),AdapterView.OnItemSelectedListener {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private lateinit var countryList : Array<CountryDetails>
    private lateinit var stateList : Array<StateDetails>
    private lateinit var cityList : Array<cityDetails>
    private lateinit var cntryId :String
    private lateinit var stateId:String
    private lateinit var cityId:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        progressBarbaseactivity2.visibility = View.GONE

        spinnerCountry.onItemSelectedListener = this
        spinnerState.onItemSelectedListener = this
        countryApi()

    }

    private fun countryApi() {
        RetrofitObject.instance.countryList()
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    when{
                        response.code() == 200 -> {
                            CountrySpinnerLoad(response)

                        }

                        response.code() == 400 -> {
                            val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                        }

                    }

                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Toast.makeText(applicationContext, "Some error",Toast.LENGTH_SHORT).show()
                }

            })



    }

    private fun StateSpinnerLoad(response: Response<JsonObject>) {

        val res = gson.fromJson(response.body().toString(), StateResponse::class.java)
        stateList = res.data.toTypedArray()
        var state = arrayOfNulls<String>(stateList.size)

        for (i in stateList.indices) {
            state[i] = stateList[i].state_name
        }
        val adapter =  ArrayAdapter(this@RegistrationActivity, android.R.layout.simple_spinner_item,state)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerState.adapter = adapter
    }

    private fun CountrySpinnerLoad(response: Response<JsonObject>) {
        val res = gson.fromJson(response.body().toString(), CountryResponse::class.java)
        countryList = res.data.toTypedArray()
        var country = arrayOfNulls<String>(countryList.size)


        for (i in countryList.indices) {
            country[i] = countryList[i].country_name

        }

        val adapter =  ArrayAdapter(this@RegistrationActivity, android.R.layout.simple_spinner_item,country)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCountry.adapter = adapter


    }
    
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent?.id) {
            R.id.spinnerCountry -> {
                cntryId = countryList.get(position).country_id

                Toast.makeText(this,cntryId,Toast.LENGTH_SHORT).show()


                RetrofitObject.instance.stateList(cntryId)
                    .enqueue(object : Callback<JsonObject>{
                        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                            when{

                                response.code() == 200 -> {

                                    StateSpinnerLoad(response)
                                }

                                response.code() == 400 -> {
                                    val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                }


                            }
                        }

                        override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                            Toast.makeText(applicationContext, "Some error",Toast.LENGTH_SHORT).show()
                        }

                    })

            }

            R.id.spinnerState -> {
                stateId = stateList.get(position).state_id
                Toast.makeText(this,stateId,Toast.LENGTH_SHORT).show()

                RetrofitObject.instance.cityList(stateId)
                    .enqueue(object : Callback<JsonObject>{
                        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                            when{

                                response.code() == 200 -> {
                                    CitySpinnerLoad(response)
                                }

                                response.code() == 400 -> {
                                    val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                }


                            }

                        }

                        override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                        }

                    })


            }

        }

    }

    private fun CitySpinnerLoad(response: Response<JsonObject>) {

        val res = gson.fromJson(response.body().toString(), CityResponse::class.java)
        cityList = res.data.toTypedArray()
        var city = arrayOfNulls<String>(cityList.size)


        for (i in cityList.indices) {
            city[i] = cityList[i].city_name

        }

        val adapter =  ArrayAdapter(this@RegistrationActivity, android.R.layout.simple_spinner_item,city)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.adapter = adapter

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }


}