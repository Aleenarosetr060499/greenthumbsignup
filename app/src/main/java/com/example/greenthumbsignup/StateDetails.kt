package com.example.greenthumbsignup

data class StateDetails (
    val state_id: String,
    val state_name: String
        )