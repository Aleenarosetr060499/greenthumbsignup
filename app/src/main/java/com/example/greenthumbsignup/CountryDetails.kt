package com.example.greenthumbsignup

data class CountryDetails(
val country_id: String,
val country_name: String
)
