package com.example.greenthumbsignup

import android.accessibilityservice.GestureDescription
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.Result.Companion.success

class MainActivity : BaseActivity() {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressBarbaseactivity.visibility = View.GONE

        LoginbtnLoginbutton.setOnClickListener {
            val email = EmailLogineditText.text.toString().trim()
            val password = PasswordLogineditText.text.toString().trim()
            val user_type: Int = 1
            callLoginApi(email, password, user_type)
        }

        signinLogintextView.setOnClickListener {
            var intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
        }
    }

    private fun callLoginApi(email: String, password: String, userType: Int) {
        showLoading()
        if(isNetworkConnected()) {

            RetrofitObject.instance.login(email, password, 1)
                .enqueue(object : Callback<JsonObject> {
                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {
                        hideLoading()

                        when {
                            response.code() == 400 -> {
                                val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_LONG).show()
                            }
                            response.code() == 200 -> {
                                val loginBase = gson.fromJson(response.body().toString(), UsersList::class.java)
                                Toast.makeText(applicationContext, loginBase.message, Toast.LENGTH_LONG).show()
                                if (loginBase.status == resources.getString(R.string.success)) {
                                    startActivity(Intent(this@MainActivity, ProfileActivity::class.java))
                                }
                            }
                            else -> {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.something_went),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }


                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    }

                })
        }
        else{
            hideLoading()
            Toast.makeText(this," check network connection", Toast.LENGTH_SHORT).show()
        }
    }
    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }
}