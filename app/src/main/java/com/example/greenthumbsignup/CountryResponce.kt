package com.example.greenthumbsignup

data class CountryResponse(
    val data: List<CountryDetails>,
    val message: String,
    val status: String
)



data class Error(val status: String?,val message: String?)