package com.example.greenthumbsignup


data class CityResponse(
    val data: List<cityDetails>,
    val message: String,
    val status: String
)